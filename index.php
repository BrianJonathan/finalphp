<!DOCTYPE html>
<?php
require_once("seccion/funciones.php");
require_once("arrays.php");
require_once("database/tatuajes.php");
//  session_start(); 
?>

<html lang="es">

<head>
  <meta charset="UTF-8">
  <title>
    Tattoo Zone
  </title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
  <div class="container">
    <div class="jumbotron jumbotron-fluid">
      <div class="shadow-sm p-3 mb-5 bg-white rounded">

        <h1 class="display-4">Tattoo Zone</h1>

        <p class="text-muted mb-4 ml-2"> El tatuaje se ha convertido en parte de la vida cotidiana y algo más allá de la moda</p>

        <?php $seccion = isset($_GET["seccion"]) ? $_GET["seccion"] : "inicio";
        include("navegacion.php"); ?>
      </div>

      <?php
      switch ($seccion) {
        case "galeria":
          $ruta = "seccion/galeria.php";
          break;
        case "contacto":
          $ruta = "seccion/contacto.php";
          break;
        case "registrarse":
          $ruta = "seccion/registrarse.php";
          break;
        case "login":
          $ruta = "seccion/login.php";
          break;
        case "registroCompleto":
          $ruta = "seccion/registroCompleto.php";
          break;
        case "inicio":
        default:
          $ruta = "seccion/inicio.php";
      }

      include($ruta); ?>


      <footer class="page-footer font-small blue pt-4">
        <div class="container-fluid text-center text-md-left">
          <hr>
          <div class="footer-copyright text-center py-3">© 2019 Posted by: Brian Gilabert
            <a href="mailto:jonathan.gilabert@davinci.edu.ar">Jonathan.gilabert@davinci.edu.ar</a>
            <br>
            <a href="mailto:jonathanbriangilabert@gmail.com">jonathanbriangilabert@gmail.com</a>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>