
    <ul class="nav nav-pills mb-4">

        <?php foreach ($nav as $ind => $valor) :
            $clase = $seccion == $valor ? "active" : "";
        ?>
            <li class="nav-item">
                <a href="index.php?seccion=<?php echo $valor; ?>" class="nav-link <?php echo $clase; ?>">
                    <?php echo $ind; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
