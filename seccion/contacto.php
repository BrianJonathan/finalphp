<h3 class="display-5">Contactanos</h3>
    <br>
    <div class="bd-callout bd-callout-info">
        <p>Hacenos llegar tus consultas o comentarios a nuestras redes o a nuestro mail: </p>
        <br>
        <a href="https://www.youtube.com/watch?v=WLGxyKoPCZc&list=RDWLGxyKoPCZc&start_radio=1">ThePiercingZone@hotmail.com</a>
    </div>
    <br>

    <div class="d-flex p-2 mb-4 bd-highlight">
        <?php foreach ($redes as $ind => $valor) : ?>

            <div class="col-md-2">
                <div class="card">
                    <img src="<?php echo $valor["imagen"]; ?>" alt="<?php echo $ind;  ?>">
                    <div class="card-body bg-dark">
                        <div class="card-text">
                            <p class="text-white"> <?php echo $valor["descripcion"]; ?> </p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

    <?php endforeach; ?>
</div>