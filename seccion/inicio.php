<?php include("slide.php"); ?>
<br>
<h2>El mundo del tatuaje </h2>
<hr>

<div class="d-flex bd-highlight panel border-primary">
    <div class="p-2 flex-fill bd-highlight">
        <p>
            Hoy en día el tatuaje representa algo diferente para cada persona, dentro de los diferentes estilos encontramos los nombres plasmados que hacen referencia a un ser querido,
            alguien a quien se admira o su propio nombre, también hay leyendas que hacen referencia a algo importante,
            una ideología o una frase con la cual quien se tatúa se siente representado o un icono admirable en la vida de cada quien.

            Por otra parte podemos encontrar representaciones gráficas de animales o flores que demuestra la admiración y respeto hacia la naturaleza y
            de cierta manera su compromiso por preservar la biodiversidad que nos ofrecen las distintas zonas geográficas en cuanto a la flora y la fauna.


            El tatuaje no cambia el carácter de las personas,
            es solo algo buscado para demostrar una serie de ideales personales y su deseo de exteriorizarlo.

        </p>
    </div>


    <div class="p-2 flex-fill bd-highlight">
        <p>
            Gran parte del avance del mundo es la tolerancia que se ha tenido al aprender a aceptar y admirar este arte,
            para los tatuadores el tema del tatuaje dejó de ser ¨Tabú¨ desde hace mucho tiempo y hasta resulta algo incomodo
            para ellos que aun hoy en día hay personas que piensan de esta manera,
            pues así como ellos afirman esto es un trabajo, y un estilo de vida sin hacerle mal a nadie.

            El tatuaje se ha convertido en parte de la vida cotidiana y algo más allá de la moda, pues para la mayoría no es
            simplemente un capricho momentáneo
        </p>
    </div>
</div>
<!-- Tabla de Horarios -->
<br>
<br>

<strong>Horarios de atencion</strong>
<table class="table">
    <tr class="text-white bg-dark">
        <th>Lunes</th>
        <th>Martes</th>
        <th>Miercoles</th>
        <th>Jueves</th>
        <th>Viernes</th>
        <th>Sabados</th>
        <th>Domingos</th>
    </tr>
    <tr>
        <td>10:00am</td>
        <td>10:00am</td>
        <td>10:00am</td>
        <td>10:00am</td>
        <td>10:00am</td>
        <td>10:00am</td>
        <td>Cerrado</td>
    </tr>

    <tr class="bg-white">
        <td>20:00pm</td>
        <td>20:00pm</td>
        <td>20:00pm</td>
        <td>20:00pm</td>
        <td>20:00pm</td>
        <td>20:00pm</td>
        <td>Cerrado</td>
    </tr>
</table>
<br>
<!-- Tabla de Horarios -->