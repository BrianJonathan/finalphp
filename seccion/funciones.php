<?php 


function crear_galeria($nombre){
   
    $carpeta = opendir("pokemones/$nombre");

    while($imagen = readdir($carpeta)):

        if($imagen != "." && $imagen != ".." && $imagen != "descripcion.txt"):
            
           return '<div class="col-xs-16 col-md-3">
                <div class="thumbnail">
                    <img src="pokemones/'. $nombre .'/'. $imagen .'" alt="'. $nombre .'" class="img-responsive">
                    <div class="caption">
                        <h4>'. ucfirst($nombre) .'</h4>
                        <p class="scrollable">'. nl2br(file_get_contents("pokemones/$nombre/descripcion.txt")) .'</p>
                    </div>
                </div>
            </div>';

        endif;

    endwhile;         
}

 function cargarImg(){ 
    
$_SESSION["img"] = [];
for($i=1;$i<=25;$i++){
        echo ("img".$i.".jpg");
        if(file_exists("galeria/imgGaleria/img".$i.".jpg")){
            
                 
        $_SESSION["img".$i]=["imagen" => "galeria/imgGaleria/img" .$i. ".jpg", "descripcion" => $i];
          
       $cantImg= $i;
            
        }
    }
} 
function limpiar_string($string){
    return htmlentities(trim(strtolower($string)));
}

function ultimoId($array){
        usort($array, function ($a, $b) {
            return $b['id'] - $a['id'];
        });
        $primerRegistro = $array[0];
        $ultimoId = $primerRegistro["id"];
    
        return $ultimoId;
    }
