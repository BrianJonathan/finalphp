
<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
  </ol>
  
  <!-- Wrapper for slides -->
  
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="galeria/imgGaleria/imgSlide3.jpg" class="d-block" alt="Chania">
      </div>
      
      <div class="carousel-item">
        <img src="galeria/imgGaleria/imgSlide2.jpg" class="d-block" alt="Chicago">
      </div>
      
      <div class="carousel-item">
        <img src="galeria/imgGaleria/imgSlide1.jpg" class="d-block" alt="New York">
      </div>
      
      
      <div class="carousel-item">
        <img src="galeria/imgGaleria/imgSlide4.jpg" class="d-block" alt="New York">
      </div>
      
      
      <div class="carousel-item">
        <img src="galeria/imgGaleria/imgSlide5.jpg" class="d-block" alt="New York">
      </div>
    </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
