<?php
include 'partials/header.php';
require __DIR__ . '/users/users.php';

if (!isset($_GET['id'])) {
    include "partials/not_found.php";
    exit;
}
$idUsuario = $_GET['id'];

$usuario = getById($idUsuario);
if (!$usuario) {
    include "partials/not_found.php";
    exit;
}

$errors = [
    'name' => "",
    'username' => "",
    'email' => "",
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario = array_merge($usuario, $_POST);

    $isValid = validarUsuario($usuario, $errors);

    if ($isValid) {
        $usuario = editarUsuario($_POST, $idUsuario);
        uploadImagen($_FILES['picture'], $usuario);
        header("Location: index.php");
    }
}

?>

<?php include '_form.php' ?>
