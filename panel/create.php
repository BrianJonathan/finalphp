<?php
include 'partials/header.php';
require __DIR__ . '/users/users.php';


$usuario = [
    'id' => '',
    'name' => '',
    'username' => '',
    'email' => '',
];

$errors = [
    'name' => "",
    'username' => "",
    'email' => "",
];
$isValid = true;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario = array_merge($usuario, $_POST);

    $isValid = validarUsuario($usuario, $errors);

    if ($isValid) {
        $usuario = crearUsuario($_POST);

        uploadImagen($_FILES['picture'], $usuario);

        header("Location: indexPanel.php");
    }
}

?>

<?php include '_form.php' ?>

