<?php

function getUsuario()
{
    return json_decode(file_get_contents(__DIR__ . '/users.json'), true);
}

function getById($id)
{
    $usuarios = getUsuario();
    foreach ($usuarios as $usuario) {
        if ($usuario['id'] == $id) {
            return $usuario;
        }
    }
    return null;
}

function crearUsuario($dato)
{
    $usuarios = getUsuario();

    $dato['id'] = rand(1000000, 2000000);

    $usuarios[] = $dato;

    putJson($usuarios);

    return $dato;
}

function editarUsuario($dato, $id)
{
    $editarUsuario = [];
    $usuarios = getUsuario();
    foreach ($usuarios as $i => $usuario) {
        if ($usuario['id'] == $id) {
            $usuarios[$i] = $editarUsuario = array_merge($usuario, $dato);
        }
    }

    putJson($usuarios);

    return $editarUsuario;
}

function eliminarUsuario($id)
{
    $usuarios = getUsuario();

    foreach ($usuarios as $i => $usuario) {
        if ($usuario['id'] == $id) {
            array_splice($usuarios, $i, 1);
        }
    }

    putJson($usuarios);
}

function uploadImagen($file, $usuario)
{
    if (isset($_FILES['picture']) && $_FILES['picture']['name']) {
        if (!is_dir(__DIR__ . "/images")) {
            mkdir(__DIR__ . "/images");
        }
        
        // Obtiene la extensión del archivo del nombre del archivo
        $nombreArchivo = $file['name'];
        // Busca el punto en el nombre del archivo
        $posicionPunto = strpos($nombreArchivo, '.');
        // Toma la subcadena desde la posición del punto hasta el final de la cadena
        $extension = substr($nombreArchivo, $posicionPunto + 1);

        move_uploaded_file($file['tmp_name'], __DIR__ . "/images/${user['id']}.$extension");

        $usuario['extension'] = $extension;
        editarUsuario($usuario, $usuario['id']);
    }
}

function putJson($usuarios)
{
    file_put_contents(__DIR__ . '/users.json', json_encode($usuarios, JSON_PRETTY_PRINT));
}

function validarUsuario($usuario, &$errors)
{
    $esValido = true;
    // Empieza validacion
    if (!$usuario['name']) {
        $esValido = false;
        $errors['name'] = 'Nombre es obligatorio';
    }
    //validar el tamaño del nombre de usuario 
    if (!$usuario['username'] || strlen($usuario['username']) < 6 || strlen($usuario['username']) > 16) {
        $esValido = false;
        $errors['username'] = 'el Username debe tener entre 6 y 16 caracteres';
    }


    if ($usuario['email'] && !filter_var($usuario['email'], FILTER_VALIDATE_EMAIL)) {
        $esValido = false;
        $errors['email'] = 'Debe ingresar email';
    }

    // termina validacion

    return $esValido;
}
