<?php
include 'partials/header.php';
require __DIR__ . '/users/users.php';


if (!isset($_POST['id'])) {
    include "partials/not_found.php";
    exit;
}
$idUsuario = $_POST['id'];
eliminarUsuario($idUsuario);

header("Location: indexPanel.php");
