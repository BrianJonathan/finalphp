<?php
require 'users/users.php';
//error_reporting(0);
$usuarios = getUsuario();

/*session_start();
$varsesion = $_SESSION['usuario'];  //Guardo variable de sesion

if($varsesion == null || $varsesion = ''){
    echo 'Usted no tiene autorización';
    die;
}*/

include 'partials/header.php';
?>


<div class="container">
    <p>
        <a class="btn btn-success" href="create.php">Crear nuevo usuario</a>
    </p>

    <table class="table">
        <thead>
        <tr>
            <th>Foto de perfil</th>
            <th>Nombre</th>
            <th>Username</th>
            <th>Email</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td>
                    <?php if (isset($usuario['extension'])): ?>
                        <img style="width: 60px" src="<?php echo "users/images/${usuario['id']}.${usuario['extension']}" ?>" alt="">
                    <?php endif; ?>
                </td>
                <td><?php echo $usuario['name'] ?></td>
                <td><?php echo $usuario['username'] ?></td>
                <td><?php echo $usuario['email'] ?></td>
                <td>
                    <a href="view.php?id=<?php echo $usuario['id'] ?>" class="btn btn-sm btn-outline-info">Ver</a>
                    <a href="update.php?id=<?php echo $usuario['id'] ?>"
                       class="btn btn-sm btn-outline-secondary">Editar</a>
                    <form method="POST" action="delete.php">
                        <input type="hidden" name="id" value="<?php echo $usuario['id'] ?>">
                        <button class="btn btn-sm btn-outline-danger">Borrar</button>
                    </form>
                </td>
            </tr>
        <?php endforeach;; ?>
        </tbody>
    </table>
</div>

</body>
</html>

