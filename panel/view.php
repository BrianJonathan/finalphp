<?php
include 'partials/header.php';
require __DIR__ . '/users/users.php';

if (!isset($_GET['id'])) {
    include "partials/not_found.php";
    exit;
}
$idUsuario = $_GET['id'];

$usuario = getById($idUsuario);
if (!$usuario) {
    include "partials/not_found.php";
    exit;
}

?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Ver usuario: <b><?php echo $usuario['name'] ?></b></h3>
        </div>
        <div class="card-body">
            <a class="btn btn-secondary" href="update.php?id=<?php echo $usuario['id'] ?>">Editar</a>
            <form style="display: inline-block" method="POST" action="delete.php">
                <input type="hidden" name="id" value="<?php echo $usuario['id'] ?>">
                <button class="btn btn-danger">Borrar</button>
            </form>
        </div>
        <table class="table">
            <tbody>
            <tr>
                <th>Nombre:</th>
                <td><?php echo $usuario['name'] ?></td>
            </tr>
            <tr>
                <th>Username:</th>
                <td><?php echo $usuario['username'] ?></td>
            </tr>
            <tr>
                <th>Email:</th>
                <td><?php echo $usuario['email'] ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


</body>
</html>
