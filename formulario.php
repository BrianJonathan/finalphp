        <form method="POST" action="seccion/upload.php" enctype="multipart/form-data">
            <!-------- Inputs ------------>
            <div class="form-group">
                <label for="nombre">Agregar tatuaje</label>
                <input type="text" class="form-control" id="nombre" placeholder="Ingrese nombre del tatuaje" name="nombre">
                <!-------- Inputs ------------>

                <!-------- Botones ------------>
                <label for="foto">Buscar</label>
                <div class="btn btn-default image-preview-input">
                    <input type="file" accept=".jpg,.png" name="foto">
                </div>
                <button type="submit" class="btn btn-default">Cargar</button>
                <!-------- Botones ------------>
            </div>
        </form>